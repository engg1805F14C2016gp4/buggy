package ryze.analytics;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class BuggyAnalyzerTest {

	
	@Test
	public void buggyanalyzer_wordDensity_wordDensityIs50(){
		List<String> worddensity = Arrays.asList(new String[]{
				"Hello world"
		}); 
		String keyword = "Hello";
		BuggyAnalyzer ba = new BuggyAnalyzer();
		assertEquals(50.00, ba.wordDensity(keyword, worddensity), 0.01);
	}
	
	@Test
	public void buggyanalyzer_wordDensity_wordDensityIs100(){
		List<String> worddensity = Arrays.asList(new String[]{
				"Hello"
		}); 
		String keyword = "Hello";
		BuggyAnalyzer ba = new BuggyAnalyzer();
		assertEquals(100.00, ba.wordDensity(keyword, worddensity), 0.01);
	}
	
	@Test
	public void buggyanalyzer_wordDensity_wordDensityIs9(){
		List<String> worddensity = Arrays.asList(new String[]{
				"Hello world, this is me, life should be fun for everyone."
		}); 
		String keyword = "Hello";
		BuggyAnalyzer ba = new BuggyAnalyzer();
		assertEquals(9.09, ba.wordDensity(keyword, worddensity), 0.01);
	}
	
	@Test
	public void buggyanalyzer_wordDensity_wordDensityIs0(){
		List<String> worddensity = Arrays.asList(new String[]{
				"I love cookies"
		}); 
		String keyword = "Hello";
		BuggyAnalyzer ba = new BuggyAnalyzer();
		assertEquals(0.00, ba.wordDensity(keyword, worddensity), 0.01);
	}
	
	@Test
	public void buggyanalyzer_wordDensity_wordDensityIs50MultipleKeywords(){
		List<String> worddensity = Arrays.asList(new String[]{
				"Hello Hello World World"
		}); 
		String keyword = "Hello";
		BuggyAnalyzer ba = new BuggyAnalyzer();
		assertEquals(50.00, ba.wordDensity(keyword, worddensity), 0.01);
	}
	
	@Test
	public void buggyanalyzer_frequentword_mostFrequentWordIsHello(){
		List<String> frequentWord = Arrays.asList(new String[]{
				"Hello, how are you?",
				"Hello, i'm good",
				"Hello"
		}); 
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(frequentWord).get(0);
		assertEquals("hello", lysis.getMostFrequentWord());
	}
	@Test
	public void buggyanalyzer_frequentword_OneWordIsHello(){
		List<String> frequentWord = Arrays.asList(new String[]{
				"Hello"
		}); 
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(frequentWord).get(0);
		assertEquals("hello", lysis.getMostFrequentWord());
	}
	
	@Test
	public void buggyanalyzer_frequentword_2WordsNoMostFrequent(){
		List<String> frequentWord = Arrays.asList(new String[]{
				"Hello world."
		}); 
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(frequentWord).get(0);
		assertEquals(null, lysis.getMostFrequentWord());
	}
	
	@Test
	public void buggyanalyzer_frequentword_2WordsMostFrequentExists(){
		List<String> frequentWord = Arrays.asList(new String[]{
				"Hello hello."
		}); 
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(frequentWord).get(0);
		assertEquals("hello", lysis.getMostFrequentWord());
	}
	
	@Test
	public void buggyanalyzer_frequentword_NoMostFrequent(){
		List<String> frequentWord = Arrays.asList(new String[]{
				"Hello from the other side."
		}); 
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(frequentWord).get(0);
		assertEquals(null, lysis.getMostFrequentWord());
	}
	
	@Test
	public void buggyanalyzer_frequentword_MostFrequentIs3Characters(){
		List<String> frequentWord = Arrays.asList(new String[]{
				"Hello, the, the"
		}); 
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(frequentWord).get(0);
		assertEquals("hello", lysis.getMostFrequentWord());
	}

	@Test
	public void buggyanalyzer_nolines_all0inbuggyanalysis(){
		List<String> noLines = new  LinkedList<>(); //here is an example on how to create input corresponding to no input lines
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(noLines).get(0);
		assertEquals(new BuggyAnalysis(0,0,0,0,0.0,0, null), lysis);
	}
	
	@Test
	public void buggyanalyzer_threeLines_totalLinesIs3(){
		List<String> threeLines = Arrays.asList(new String[]{"hello", "beautiful", "world"}); // an example of short input
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(threeLines).get(0);
		assertEquals(3, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_multLines_totalLinesIs7(){
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"times, it was the worst",
				"of times",
		});
	
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		assertEquals(7, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_wordDelimitation_totalWordsIs7(){ //TODO change test name
		List<String> wordDelimitation = Arrays.asList(new String[]{
				"Apple",
				"Apple,Book",
				"Apple Book",
				"Apple, Book",
				"AppleBook",
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordDelimitation).get(0); 
		assertEquals(7, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_wordCount_wordCountIs5(){ //TODO change test name
		List<String> wordCount = Arrays.asList(new String[]{
				"Let's make a long sentence.",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordCount).get(0); 
		assertEquals(5, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_totalWordCount_wordCountIs13(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalWordCount = Arrays.asList(new String[]{
				"Lets make a long sentence",
				"But",
				"Lets like do it over multiple lines",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalWordCount).get(0); 
		assertEquals(13, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_avgLineLength_avgLineIs3(){ //TODO change test name
		//TODO Construct your own data 
		List<String> avgLineLen = Arrays.asList(new String[]{
				"Hello World",
				"I like writing words",
				"Words are cool",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(avgLineLen).get(0); 
		assertEquals( 
			3.0, // TODO change to your_expected_value> 
			lysis.getAvgLineLength(),
			0.01 // we accept an error 0.01 and less
		);
	}
	@Test
	public void buggyanalyzer_minLineLength_minLineIs2(){ //TODO change test name
		//TODO Construct your own data 
		List<String> minLineLen= Arrays.asList(new String[]{
				"Hello World",
				"I like writing words",
				"Words are cool",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(minLineLen).get(0); 
		assertEquals(2, lysis.getMinLineLength());
	}
	@Test
	public void buggyanalyzer_maxLineLength_maxLineIs4(){ //TODO change test name
		//TODO Construct your own data 
		List<String> maxLineLen= Arrays.asList(new String[]{
				"Hello World",
				"I like writing words",
				"Words are cool",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(maxLineLen).get(0); 
		assertEquals(4, lysis.getMaxLineLength());
	}
	@Test
	public void buggyanalyzer_sentenceCount_1(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(1, lysis.getTotalSentences());
	}
	@Test
	public void buggyanalyzer_sentenceCount_5(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World. I am Alex. Nice to meet you. Another sentence. One more."
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(5, lysis.getTotalSentences());
	}
	@Test
	public void buggyanalyzer_sentenceCount_variedPunctuation(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World. How are you? Nice to meet you! Another sentence. One more."
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(5, lysis.getTotalSentences());
	}
	@Test
	public void buggyanalyzer_sentenceCount_elipses(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World... How are you? Nice to meet you! Another sentence. One more."
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(5, lysis.getTotalSentences());
	}
	@Test
	public void buggyanalyzer_sentenceCount_multLines(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World.",
				"How are you?",
				"Nice to meet you!",
				"Another sentence.",
				"One more."
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(5, lysis.getTotalSentences());
	}
	@Test
	public void buggyanalyzer_sentenceCount_newLine(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World",
				"How are you? Nice to meet you! Another sentence. One more."
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(5, lysis.getTotalSentences());
	}
	@Test
	public void buggyanalyzer_sentenceCount_newLineNoPunctuation(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalSentences= Arrays.asList(new String[]{
				"Hello World",
				"How are you",
				"Nice to meet you",
				"Another sentence",
				"One more",
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalSentences).get(0); 
		assertEquals(5, lysis.getTotalSentences());
	}
}