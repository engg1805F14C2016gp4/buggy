package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import ryze.analytics.BuggyAnalysis;

public class BuggyBillerTest {

	@Test
	public void buggybiller_allZeroAnalysis_0(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(0,0,0,0,0.0,0,null);
		assertEquals(0.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}

	@Test
	public void buggybiller_lowerRate_billIs165(){ //TODO change test name
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,55,10,1,5.5,0,null);
		assertEquals(165.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}
	
	@Test
	public void buggybiller_higherRate_billIs406() {//TODO change test name
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,58,10,1,5.8,0,null);
		assertEquals(406.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}
}
