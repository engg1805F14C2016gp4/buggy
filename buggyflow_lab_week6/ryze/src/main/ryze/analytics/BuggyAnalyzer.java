package ryze.analytics;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class BuggyAnalyzer implements Analyzer<String, BuggyAnalysis>{

	@Override
	public List<BuggyAnalysis> analyze(List<String> lines) {
		List<BuggyAnalysis> res = new LinkedList<>();
		
		BuggyAnalysis lysis = new BuggyAnalysis();
		res.add(lysis);
		
		lysis.setTotalLines(calcTotalLines(lines));
		lysis.setTotalWords(calcTotalWords(lines));
		lysis.setMinLineLength(calcMinLineLength(lines));
		lysis.setMaxLineLength(calcMaxLineLength(lines));
		lysis.setAvgLineLength(calcAvgLineLength(lines));
		lysis.setTotalSentences(calcTotalSentences(lines));
		lysis.setMostFrequentWord(mostFrequentWord(lines));
		return res;
	}
	
	public double wordDensity(String keyword, List<String> lines){
		keyword = keyword.toLowerCase();
		//puts all of the words into an array
		int count = 0;
		String[] words = new String[calcTotalWords(lines)];
		for (int i = 0; i < lines.size(); i ++){
			String line = lines.get(i);
			String[] splitLine = line.split(" ");
			for (int j = 0; j < splitLine.length; j ++){
				words[count] = splitLine[j];
				count ++;
			}
			
		}
		
		//removes punctuation from the end of words
		for (int i = 0; i < words.length; i++){
			
			while (true){
				char lastLetter = words[i].charAt(words[i].length() - 1);
				if (!((lastLetter >= 65 && lastLetter <= 90) || (lastLetter >= 97 && lastLetter <= 122))){
					words[i] = words[i].substring(0, words[i].length() - 1);
					
				} else {
					break;
				}
			}
		}
		double keywordCount = 0;
		//sets words to lower case and sorts them
		//loops through words and increments a counter variable when keyword is found
		for (int i = 0; i < words.length; i ++){
			words[i] = words[i].toLowerCase();
			if(words[i].equals(keyword)){
				keywordCount ++;
			}
		}
		
		double density = (keywordCount/words.length) * 100;
		
		density = Math.round(density * 100);
		density = density/100;
		return density;
	}
	
		
	private String mostFrequentWord(List<String> lines){
		int count = 0;
		String[] words = new String[calcTotalWords(lines)];
		for (int i = 0; i < lines.size(); i ++){
			String line = lines.get(i);
			String[] splitLine = line.split(" ");
			for (int j = 0; j < splitLine.length; j ++){
				words[count] = splitLine[j];
				count ++;
			}
			
		}
		
		for (int i = 0; i < words.length; i++){
			
			while (true){
				char lastLetter = words[i].charAt(words[i].length() - 1);
				if (!((lastLetter >= 65 && lastLetter <= 90) || (lastLetter >= 97 && lastLetter <= 122))){
					words[i] = words[i].substring(0, words[i].length() - 1);
					
				} else {
					break;
				}
			}
		}
		for (int i = 0; i < words.length; i ++){
			words[i] = words[i].toLowerCase();
		}
		Arrays.sort(words);
		if (words.length == 1){
			return words[0];
		} else if (words.length == 2){
			if (words[0].equals(words[1])){
				return words [0];
			} else {
				return null;
			}
		}
		int amountOfCurrent = 1;
		int amountOfMode = 0;
		int location = 0;
		int amountOfLargest = 0;


		for (int i = 0; i < words.length - 1; i ++) {
			if (words[i].length() < 4){
				continue;
			}
			if (words[i].equals(words[i + 1])){
				amountOfCurrent += 1;
			} else {
				if (amountOfCurrent >= amountOfMode) {
					if (amountOfCurrent == amountOfMode) {
						amountOfLargest += 1;
						amountOfCurrent = 1;
					} else {
						amountOfMode = amountOfCurrent;
						location = i;
						amountOfLargest = 0;
					}

				} else {
					amountOfCurrent = 1;
				}
				
			}
		}
		
		if (amountOfMode == 0) {
			return null;
		} else if (amountOfLargest > 0){
			return null;
		} else {
			return words[location];
		}
		
		
		
		
	}
	
	private int calcTotalSentences(List<String> lines) {
		int sentenceCount = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			for(int j=0; j<words.length; j++){
				String current = words[j];
				if(current.charAt(current.length() -1) == '.' ||
				   current.charAt(current.length() -1) == '?' ||
				   current.charAt(current.length() -1) == '!'){
					sentenceCount ++;
				}
				else if(j == words.length -1){
					sentenceCount ++;
				}
			}
		}
		return sentenceCount;
	}
	
	private double calcAvgLineLength(List<String> lines) {
		if(calcTotalLines(lines) == 0){
			return 0.0;
		}
		return calcTotalWords(lines)/calcTotalLines(lines);
	}

	private int calcMaxLineLength(List<String> lines) {
		int maxLineLen = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			int lineLen = 0;
			for(int j=0; j<words.length; j++){
				lineLen++;
			}
			if(lineLen > maxLineLen ){
				maxLineLen = lineLen;
			}
		}
		return maxLineLen;
	}

	private int calcMinLineLength(List<String> lines) {
		int minLineLen = 0;
		for(int i=0; i<lines.size(); i++){
			
			String line = lines.get(i);
			String[] words = line.split(" ");
			int lineLen = 0;
			for(int j=0; j<words.length; j++){
				lineLen++;
			}
			if(i ==0){
				minLineLen = lineLen;
			}
			else if(lineLen < minLineLen ){
				minLineLen = lineLen;
			}
		}
		return minLineLen;
	}

	private int calcTotalWords(List<String> lines) {
		int wordcount = 0;
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			for(int j=0; j<words.length; j++){
				wordcount = wordcount + 1;
			}
		}
		return wordcount;
	}

	private int calcTotalLines(List<String> lines) {
		int count = 0;
		for(int i=0; i<lines.size();i++){
			count = count + 1;
			
		}
		return count;
	}

}
